package com.example.customview;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class CircleView extends View{
	
	Paint p = new Paint();
	Rect bounds = new Rect(); 
	public CircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		p.setAntiAlias(true);
		p.setColor(Color.BLUE);
		// Set text size to 24 dp
		p.setTextSize(20);
		// Set the origin of the string to be the bottom-left corner
		//p.setTextAlign(1.0);
	
	}
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		//canvas object is used to draw the graphic
		//can be used inside onDraw method only
		super.onDraw(canvas);
		
		
		// Get the dimension of this view
		float viewWidth = this.getWidth(); 
		float viewHeight = this.getHeight();
		
	
		

		//draw the circle with radius 50,50
		canvas.drawCircle(100,100,50,p);
		
		
		
		
		
	}

	
	

}

