package com.example.customview;

import java.util.ArrayList;
import java.util.Random;

import android.R.color;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

//need at least a constructor
public class MyTextView extends View {
	ArrayList<Circle> circles = new ArrayList<Circle>();
	float x,y;
	Paint p = new Paint();
	Rect bounds = new Rect();  //rectangle for store the dimension of text
	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		p.setAntiAlias(true);
		

		
	}
	
	
	@Override
	//This method will be used todraw (display some contents) our view
	//when Andriod try to show it to user.
	public void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		//canvas object is used to draw the graphic
		//can be used inside onDraw method only
		super.onDraw(canvas);
		for(Circle c:circles){
		p.setColor(c.color);
			canvas.drawCircle(c.x,c.y,20,p);
		}
		//draw the circle with radius 50,50
		
		
	}
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
			circles.add(new Circle(event.getX(),event.getY(),randomColor()));
			x = event.getX();
			y = event.getY();
			invalidate();
			
		
		
		return true;
	}

	private int randomColor(){
		Random r = new Random();
		float[] hsv = new float[3];
		hsv[1] =  (float) 1.0;
		hsv[2] =  (float) 1.0;
		hsv[0] = r.nextInt(360);
		int c = Color.HSVToColor(hsv);
		return c;
	}
}

class Circle{
	float x,y;
	int color;
	public Circle(float x, float y, int c){
		this.x =x;
		this.y =y;
		this.color = c;
	}
}
